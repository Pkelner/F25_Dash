Welcome to F25 Raspberry Pi Dashboard!
======

In this Repository, you will find all the code meant to run on a Raspberry Pi running the Raspbian Linux Distribution. 

This readme is currently a living document, and will be updated as the code in the repository is pushed to the master branch. 

If any code in the master branch is still confusing even with the comments in the code or with this readme, contact the following:

>phill.kelner@gmail.com

OR

>pmk2867@rit.edu


Initial Setup
-----
For right now, the directory that the scripts must all live in is the following:

```shell
~/dev/F25_Dash
```

There are several scripts to install in the "installation_scripts" directory. The following needs to be done on initial setup.

```shell
sudo cp ~/dev/F25_Dash/installation_scripts/* /etc/system/systemd
sudo systemctl daemon-reload
```

After the systemctl has been reloaded, the scripts should launch successfully on bootup. However, first the frontend needs to be compiled. This is done with the following command:

```shell
sudo ./createBin.sh
```

Modifying the program
-----
At the moment, the createBin.sh script compiles the index.html file that runs javascript along with the nwjs binaries and the Formula Logo.
If ever any changes are made to the index.html file, the changes will not be reflected until createBin.sh has been re-run.

The hardware.py and parser.py script will have their changes reflected as soon as their respective services are restarted, or the system is rebooted.

Basic Functionality of the Program
-----
The hardware script is the first line of interaction between the world and the dashboard. This script handles communication on the CAN bus, as well as any GPIO interactions,
such as:

* Shift Paddles
* Toggle Switches
* Control Knobs

The parser script contacts the hardware script via a UDP socket, and receives data the hardware script has received via CAN. 

The parser then turns the data into commands for the frontend. 

The frontend then uses the JavaScript Graphics Library (JSGL) to render the screen via Node.js