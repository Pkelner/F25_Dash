import binascii
import socket
import struct
import sys
import signal
import RPi.GPIO as GPIO
import time
import threading
import json
import os
import logging
from datetime import date, datetime

today = datetime.today()

logging.basicConfig(filename='/home/dash/logs/{}_{}_{}.log'.format(str(today.day),
                                                                   str(today.month),
                                                                   str(today.year)), level=logging.DEBUG)

#TODO: FIX DUAL SHIFTING PROBLEM

current_time = lambda: int(round(time.time() * 1000))
timeNow = lambda: '({}:{}:{}): '.format(datetime.now().time().hour,
                                        datetime.now().time().minute,
                                        datetime.now().time().second)

watchDogTimer = current_time() + 30

# Definitions of script-wide constants, such as device CAN IDs.
Ctrlr_ID={"ECU_IDprim":0x00FF,
          "ECU_IDsec" :0x00FC,
          "E1_Current":0x00A6,
          "E1_Motor"  :0x00A8,
          "E1_BMS_1"  :0x05A0}
messageCANID = 0x05EB
shiftCANID = 0x0610
driverCANID = 0x05EF

shiftHoldTime = 0.2

watchDogMutex = threading.Lock()
shiftMutex = threading.Lock()

# Safe program exit handler.
def signal_handler(signal, frame):
    global dataSocket

    logging.info('Signal handler entered. Closing server...')
    #dataSocket.detach()
    dataSocket.shutdown(socket.SHUT_RDWR)
    dataSocket.close()
    GPIO.cleanup()
    logging.info('GPIO Cleaned, server closed.')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# Initialize the dictionary object that will become the message packet passed
# to the parser script.
jsonDict = {}

# Set a default speed lockout.
speedLockout = 10

# Setting the GPIO addressing mode to be the Broadcom SOC Channel.
GPIO.setmode(GPIO.BCM)

# Creating and defaulting all the CAN Messages, and setting their IDs so that
# the rest of the network will recognize the Pi.
shiftMessage = bytearray([0x00,
                          0xFF,
                          0xFF,
                          0xFF,
                          0xFF,
                          0xFF,
                          0xFF,
                          0xFF])

canStatus = bytearray([0x00,
                       0x00,
                       0x00,
                       0x00,
                       0x00,
                       0x00,
                       0x00,
                       0x00])

# Initializing the driver settings from the stored settings from previous run.
with open('settings.json') as settings:
    driverSettings = json.load(settings)

driverStatus = bytearray([])


# This probably needs to change to agree with Bosch's CAN templates.
for key in driverSettings:
    driverStatus.append(int(driverSettings[key], 0))

"""
Stores a changed setting into the settings file.

@param key = The key title in the settings file to revise.
@param value = The new string value that needs to get stored away.
"""
def storeSetting(key, value):
    newValue = int(value)
    print (newValue)
    strValue = '0x' + str(newValue)#.decode("utf-8")
    print (strValue)
    driverSettings[key] = strValue
    with open('settings.json', 'w') as settings:
        settings.seek(0)
        json.dump(driverSettings, settings, indent=4)


# CAN frame packing/unpacking
can_frame_fmt = "=IB3x8s"

def build_can_frame(can_id, data):
    can_dlc = len(data)
    data = data.ljust(8, b'\x00')
    return struct.pack(can_frame_fmt, can_id, can_dlc, data)

def dissect_can_frame(frame):
    can_id, can_dlc, data = struct.unpack(can_frame_fmt, frame)
    return (can_id, can_dlc, data[:can_dlc])

canSocket = socket.socket(socket.AF_CAN, socket.SOCK_RAW, socket.CAN_RAW)
canSocket.bind(("can0",))

# Establish the UDP socket that will be listening for queries from the parser
# script, as well as status updates from the GUI menus.
host = ''
port = 50000
backlog = 5
size = 1024

dataSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dataSocket.bind((host,port))
dataSocket.listen(backlog)

'''
Defines a Shift Paddle. Arguments are which pin it listens to for its input,
which pin it uses to send a digital output to Racepack, and the direction of
the paddle (up or down) as either 1 or 0.

@param self = Required self object
@param pin = The GPIO pin number to read this paddle from.
@param outPin = The GPIO pin number to directly command based on the paddle.
@param direction = Boolean value to decide if this is the up or down (1 or 0)
'''
class ShiftPaddle:
    def __init__(self, pin, outPin, direction):
        self.pin = pin
        self.out = outPin
        self.upOrDn = direction
        self.prevEnd = current_time()

        # Set input pin and configure with internal pull-up.
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # Set output pin.
        GPIO.setup(outPin, GPIO.OUT)
        # Set input pin interrupt to service the object's member function.
        GPIO.add_event_detect(pin, GPIO.FALLING, callback=self.paddleCallback, bouncetime=200)

    def shiftOutput(self):
        global shiftMutex
        global shiftMessage

        if self.upOrDn:
            if jsonDict["GEAR"] != 4:
                # Check to make sure the paddle is still pressed even after the
                # interrupt has fired.
                if GPIO.input(self.pin) == GPIO.LOW:
                    # Wait 5ms before checking again.
                    time.sleep(0.005)

                    # Check one more time that the paddle is still pressed. If
                    # so, execute the shift.
                    if GPIO.input(self.pin) == GPIO.LOW:
                        # Set the shiftMessage to tell the ECU which paddle was pressed.
                        shiftMutex.acquire()
                        shiftMessage[0] |= 0x02
                        shiftMutex.release()

                        # Set the output signal to Racepak high.
                        GPIO.output(self.out, GPIO.HIGH)
                        # Keep it high for 200ms.
                        time.sleep(shiftHoldTime)
                        # Return the signal low again.
                        GPIO.output(self.out, GPIO.LOW)

                # Wait until the input has gone high (i.e. released paddle)
                # before indicating the end of a shift.
                while GPIO.input(self.pin) == GPIO.LOW:
                    self.prevEnd = current_time()
                logging.info("{} UpShift Completed.".format(timeNow()))
        else:
            if jsonDict["GEAR"] == 1:
                if jsonDict["SPEED"] < speedLockout:
                    # Check to make sure the paddle is still pressed even after
                    # the interrupt has fired.
                    if GPIO.input(self.pin) == GPIO.LOW:
                        # Wait 5ms before checking again.
                        time.sleep(0.005)

                        # Check one more time that the paddle is still pressed.
                        # If so, execute the shift.
                        if GPIO.input(self.pin) == GPIO.LOW:
                            # Set the shiftMessage to tell the ECU which paddle was pressed.
                            shiftMutex.acquire()
                            shiftMessage[0] |= 0x01
                            shiftMutex.release()

                            # Set the output to Racepak high.
                            GPIO.output(self.out, GPIO.HIGH)
                            # Hold the output high for 200ms.
                            time.sleep(shiftHoldTime)
                            # Return the output low.
                            GPIO.output(self.out, GPIO.LOW)

                    while GPIO.input(self.pin) == GPIO.LOW:
                        self.prevEnd = current_time()
                    logging.info("{} DownShift Completed.".format(timeNow()))
            elif jsonDict["GEAR"] >= 0:
                if GPIO.input(self.pin) == GPIO.LOW:
                    # Wait 5ms before checking again.
                    time.sleep(0.005)

                    # Check one more time that the paddle is still pressed.
                    # If so, execute the shift.
                    if GPIO.input(self.pin) == GPIO.LOW:
                        # Set the shiftMessage to tell the ECU which paddle was pressed.
                        shiftMutex.acquire()
                        shiftMessage[0] |= 0x01
                        shiftMutex.release()

                        # Set the output to Racepak high.
                        GPIO.output(self.out, GPIO.HIGH)
                        # Hold the output high for 200ms.
                        time.sleep(shiftHoldTime)
                        # Return the output low.
                        GPIO.output(self.out, GPIO.LOW)

                while GPIO.input(self.pin) == GPIO.LOW:
                    self.prevEnd = current_time()
                logging.info("{} DownShift Completed.".format(timeNow()))

    def paddleCallback(self, channel):
        global shiftMutex
        global shiftMessage
        if current_time() - self.prevEnd > 50:
            logging.info("{} Shift Demanded.".format(timeNow())) 
            shiftCtrlThread = threading.Thread(target=self.shiftOutput)
            shiftCtrlThread.start()

'''
The class to configure a DPAD object.

@param self = Required self object.
@param up = Pin to configure the up button to.
@param dn = Pin to configure the down button to.
@param left = Pin to configure the left button to.
@param right = Pin to configure the right button to.
'''
class DPAD:
    def __init__(self, up, dn, left, right):
        self.pinU = up
        self.pinD = dn
        self.pinL = left
        self.pinR = right

        # Define the correct pins to be inputs with Pull-Up Resistors
        GPIO.setup(self.pinU, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pinD, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pinL, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pinR, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        
        # Configure each pin to a custom function.
        GPIO.add_event_detect(self.pinU, GPIO.FALLING, callback=self.upPress, bouncetime=500)
        GPIO.add_event_detect(self.pinD, GPIO.FALLING, callback=self.dnPress, bouncetime=500)
        GPIO.add_event_detect(self.pinL, GPIO.FALLING, callback=self.leftPress, bouncetime=500)
        GPIO.add_event_detect(self.pinR, GPIO.FALLING, callback=self.rightPress, bouncetime=500)

    def upPress(self, channel):
        if jsonDict["SPEED"] < speedLockout:
            jsonDict["DPAD"] = "UP"

    def dnPress(self, channel):
        if jsonDict["SPEED"] < speedLockout:
            jsonDict["DPAD"] = "DOWN"

    def leftPress(self, channel):
        if jsonDict["SPEED"] < speedLockout:
            jsonDict["DPAD"] = "OUT"

    def rightPress(self, channel):
        if jsonDict["SPEED"] < speedLockout:
            jsonDict["DPAD"] = "IN"

'''
A class to define a physical knob that exists on the real dash panel.

@param self = Required self object.
@param pin1 = First pin that decides the knob status.
@param pin2 = Second pin that decides the knob status.
@param byte = The index of the canStatus message that this knob controls.
'''
class Knob:
    def __init__(self, pin1, pin2, byte):
        self.pinA = pin1
        self.pinB = pin2
        self.canByte = byte

        GPIO.setup(self.pinA, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pinB, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        GPIO.add_event_detect(self.pinA, GPIO.BOTH, callback=self.knobA, bouncetime=500)
        GPIO.add_event_detect(self.pinB, GPIO.BOTH, callback=self.knobB, bouncetime=500)

    '''
    Clears bit 0 if Pin1 is now low. Sets it if the pin is now high.
    '''
    def knobA(self, channel):
        if GPIO.input(channel) == GPIO.LOW:
            canStatus[self.canByte] = canStatus[self.canByte] & 0xFE
        elif GPIO.input(channel) == GPIO.HIGH:
            canStatus[self.canByte] = canStatus[self.canByte] | 0x01

        canSocket.send(build_can_frame(statusCANID, bytes(canStatus)))
    '''
    Clears bit 1 if Pin2 is now low. Sets it if the pin is now high.
    '''
    def knobB(self, channel):
        if GPIO.input(channel) == GPIO.LOW:
            canStatus[self.canByte] = canStatus[self.canByte] & 0xFD
        elif GPIO.input(channel) == GPIO.HIGH:
            canStatus[self.canByte] = canStatus[self.canByte] | 0x02

        canSocket.send(build_can_frame(statusCANID, bytes(canStatus)))
'''
The class to deal with a switch toggling.

@param self = Requried self object.
@param switchPin = The GPIO pin that the switch is connect to.
@param byte = The canStatus byte that is controlled by the switch.
'''
class ToggleSwitch:
    def __init__(self, switchPin, byte):
        self.pin = switchPin
        self.canByte = byte
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        GPIO.add_event_detect(self.pin, GPIO.BOTH, callback=self.switch, bouncetime=500)

    def switch(self, channel):
        if GPIO.input(channel) == GPIO.LOW:
            canStatus[self.canByte] = canStatus[self.canByte] & 0x00
        elif GPIO.input(channel) == GPIO.HIGH:
            canStatus[self.canByte] = canStatus[self.canByte] | 0x01

        canSocket.send(build_can_frame(MessageCANID, bytes(canStatus)))

# This is the pin number that the CAN shield uses for the interrupt.
#CANInterrupt = 25

# Configure the pin that the CAN Interrupt exists on as an input.
#GPIO.setup(CANInterrupt, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

'''
Interrupt service routine for when a new CAN Message has arrived.
'''
# TODO: Possibly spin this out to a threaded service? It might happen often
# enough to slow down the rest of the system. This has been untested.
def CANInputRX():

    senderData = bytes([])
    cf, addr = canSocket.recvfrom(16)
    senderID, senderDLC, senderData = dissect_can_frame(cf)

    while senderID not in Ctrlr_ID.values():

        senderData = bytes([])
        cf, addr = canSocket.recvfrom(16)
        senderID, senderDLC, senderData = dissect_can_frame(cf)

    if senderID == Ctrlr_ID["ECU_IDprim"]:
        jsonDict["RPM"] = int((senderData[0] << 8) + senderData[1])
        #print ("RPM is currently: " + str(jsonDict["RPM"]))
        jsonDict["OILT"] = int(senderData[2])
        #print ("OILT is currently: " + str(jsonDict["OILT"]))
        jsonDict["WATERT"] = int(senderData[3])
        #print ("WATERT is currently: " + str(jsonDict["WATERT"]))
        jsonDict["OILP"] = int(senderData[4])/10
        #print ("OILP is currently: "  + str(jsonDict["OILP"]))
        jsonDict["GEAR"] = int(senderData[5])
        jsonDict["SPEED"] = int(senderData[6] << 8 + senderData[7])/100
    elif senderID == Ctrlr_ID["ECU_IDsec"]:
        jsonDict["BATT"] = int((senderData[0] << 8) + senderData[1])/1000
        jsonDict["AIRT"] = int(senderData[2] + senderData[3])
    elif senderID == Ctrlr_ID["E1_Current"]:
        jsonDict["I_CURRENT"] = int(senderData[6] + senderData[7])
    elif senderID == Ctrlr_ID["E1_Motor"]:
        jsonDict["ANG_V"] = int(senderData[2] + senderData[3])
    elif senderID == Ctrlr_ID["E1_BMS_1"]:
        jsonDict["P_SOC"] = int(senderData[0])
        jsonDict["P_VOLTS"] = int(senderData[2] + senderData[3])
        jsonDict["P_TEMP"] = int(senderData[4] + senderData[5])

# Configure the interrupt vector.
#GPIO.add_event_detect(CANInterrupt, GPIO.RISING, callback=CANInputRX)

# Build each GPIO object below.

upShift = ShiftPaddle(4, 22, 1) # Input = Pin4, Output = Pin22, Direction=Up
dnShift = ShiftPaddle(5, 6, 0) # Input = Pin5, Output = Pin6, Direction=Down

# Up = Pin16, Down = Pin17, Left = Pin18, Right = Pin19
#steerDPad = DPAD(16, 17, 18, 19)

#knob1 = Knob(20, 21, 0) # PinA = Pin20, PinB = Pin21, Byte = Byte0
#knob2 = Knob(23, 24, 1) # PinA = Pin23, PinB = Pin24, Byte = Byte1
#knob3 = Knob(26, 27, 2) # PinA = Pin26, PinB = Pin27, Byte = Byte2

#toggle1 = ToggleSwitch(12, 3) # TogglePin = Pin12, Byte = Byte3
#toggle2 = ToggleSwitch(13, 4) # TogglePin = Pin13, Byte = Byte4


# Default to 1, but ideally will be updated as soon as the ECU sends something.
jsonDict["GEAR"] = 1
jsonDict["SPEED"] = 5



def shiftOutputCAN():
    global shiftMessage
    global shiftMutex
    global watchDogMutex
    global watchDogTimer
    counter = 0

    while True:
        # Lock down the shiftMessage.
        shiftMutex.acquire()

        canSocket.send(build_can_frame(shiftCANID, shiftMessage))

        if shiftMessage[0] != 0x00:
            counter += 1

        if counter >= 50:
            counter = 0
            shiftMessage[0] = 0x00

        shiftMutex.release()

        watchDogMutex.acquire()
        watchDogTimer = current_time()
        watchDogMutex.release()

        time.sleep(0.002)

shiftThread = threading.Thread(target=shiftOutputCAN)
shiftThread.start()
logging.info("{} Shift Thread Started".format(timeNow()))

def watchDog():
    global watchDogMutex
    global watchDogTimer
    global dataSocket

    while True:
        watchDogMutex.acquire()
        if (current_time() - watchDogTimer > 30):
            logging.warning("{} Watchdog has tripped! Resetting...".format(timeNow()))
            dataSocket.shutdown(socket.SHUT_RDWR)
            dataSocket.close()
            GPIO.cleanup()
            logging.warning("{} Watchdog - GPIO Cleared".format(timeNow()))
            os._exit(1)
        watchDogMutex.release()
        time.sleep(0.03)

watchDogThread = threading.Thread(target=watchDog)
watchDogThread.start()
logging.info("{} Watchdog Thread Started".format(timeNow()))

while True:
    # Hardware script will wait here until a client makes a connection and then
    # identifies itself.
    client, address = dataSocket.accept()
    data = client.recv(size).decode('utf-8')

    if data:
        serverUsed = True

        # If the parser has connected, it's waiting for fresh data. Send some.
        if data == 'Data Parser':

            CANInputRX()

            # Dump the dictionary to a string-like object and send it.
            packet = json.dumps(jsonDict)
            client.send(packet.encode('utf-8'))

            # Clear the DPAD entry so that the menu isn't endlessly spinning.
            jsonDict["DPAD"] = ""

        # TODO: Is this gonna be a thing?
        # elif data == 'Telemetry Client':
        #    clientDict['Viewer'] = client
        #    print("Connection from viewer")

        # In this case, the message is probably from the front-end, so
        # service its status update from the menu.
        # TODO Remove all print status messages. They take extra time.
        else:
            jsonRecv = json.loads(data)
            print("Connection from driver")
            print("Received: " + str(jsonRecv))
            if jsonRecv['Menu'] == 'Wet/Dry Toggle':
                driverStatus[0] = driverStatus[0] ^ 0x01
                storeSetting("Wet Toggle", driverStatus[0])
            elif jsonRecv['Menu'] == 'Blip Downshift Toggle':
                driverStatus[1] = driverStatus[1] ^ 0x01
                storeSetting("Blip Toggle", driverStatus[1])
            elif jsonRecv['Menu'] == 'Launch Enable Toggle':
                driverStatus[2] = driverStatus[2] ^ 0x01
                storeSetting("Launch Toggle", driverStatus[2])
            elif jsonRecv['Menu'] == 'Active System Toggle':
                driverStatus[3] = driverStatus[3] ^ 0x01
                storeSetting("Active Bias Toggle", driverStatus[3])
            elif jsonRecv['Menu'] == 'DRS Toggle':
                driverStatus[4] = driverStatus[4] ^ 0x01
                storeSetting("DRS Toggle", driverStatus[4])
            elif jsonRecv['Menu'] == 'Rain':
                driverStatus[5] = 0x01
                storeSetting("Bias Map", driverStatus[5])
            elif jsonRecv['Menu'] == 'Hot':
                driverStatus[5] = 0x02
                storeSetting("Bias Map", driverStatus[5])
            elif jsonRecv['Menu'] == 'Cold':
                driverStatus[5] = 0x03
                storeSetting("Bias Map", driverStatus[5])
            elif jsonRecv['Menu'] == 'Speed':
                speedLockout = int(jsonRecv['Speed'])
                storeSetting("Speed", speedLockout)
            elif jsonRecv['Menu'] == 'Brightness':
                brightness = jsonRecv['Brightness']
                storeSetting("Brightness", brightness)

