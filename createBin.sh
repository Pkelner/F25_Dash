#!/bin/bash
# This script will create the nwjs executable.

echo "Packaging NW files"

zip package.nw index.html package.json jsgl.min.js ritracing.png 

if [ ! -d "nwjs" ]; then
    git clone https://github.com/jalbam/nwjs_rpi.git nwjs
fi

mv package.nw nwjs
cd nwjs
cat nw package.nw > dash_bin
chmod 755 dash_bin

echo "Files packaged"
