import socket
import asyncio
import websockets
import struct
import sys
import signal
import RPi.GPIO as GPIO
import time
import threading
import json
import queue

def signal_handler(signal, frame):
    print('Parser closing server and client connect')
    #webSocket.close()
    print('Parser server closed.')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# Initialize some useful global variables.
messageList = []
lightsBrightNow = 0
gearPrev = 0
prev_V = 0


'''
An object mapped to a bar on the front-end script.

@param self = Required self object.
@param barname = Title of the bar, displayed below it.
@param barHeight = the number of blocks the bar is tall.
@param value = The initialization value.
@param low = the lowest value the bar should even bother displaying.
@param high = the highest value the bar should bother displaying.
'''
class Bar:
    def __init__(self, barName, barHeight, value, low, high):
        self.barName = barName
        self.barHeight = barHeight
        self.value = value
        self.low = low
        self.high = high

        self.scalar = (high - low) / barHeight

        self.previousBars = 0
    '''
    A funciton to update the bar with a new value received from hardware.

    @param self = Required self object.
    @param newValue = the new value to parse and send forward.
    '''
    def updateBar(self, newValue):
        # Message to send as a dict. Object is a barName, and its value.
        message = {self.barName : "", "Value": newValue}
        messageList.append(message)
        self.value = newValue

        if newValue >= self.low:
            currentBars = int((newValue - self.low) / self.scalar)

            if currentBars != self.previousBars:
                if currentBars < self.previousBars:
                    index = self.previousBars - 2
                    while index >= currentBars:
                        message = {self.barName : "",
                                   "Index" : index,
                                   "Color" : 'gray'}
                        messageList.append(message)
                        index -= 1
                elif currentBars > self.previousBars:
                    index = self.previousBars
                    while index < currentBars:
                        if index < (self.barHeight / 4):
                            message = {self.barName : "",
                                       "Index" : index,
                                       "Color" : 'blue'}
                        elif index < ((self.barHeight / 4) * 3):
                            message = {self.barName : "",
                                       "Index" : index,
                                       "Color" : 'green'}
                        else:
                            message = {self.barName : "",
                                       "Index" : index,
                                       "Color" : 'red'}
                        messageList.append(message)
                        index += 1
            self.previousBars = currentBars

'''
Function to compute the tachometer's new display.

@param RPM = new RPM value from hardware to compute.
'''
def sendRPM(RPM):
    # TODO: Why do these need to be global? Asynchronus stuff?
    global messageList
    global lightsBrightNow

    # Do the lifting for the driver interface.
    # This keeps track of how many lights are on, and only sends the necessary
    # number of packets to change the necessary number of lights.
    lightsBrightPrev = lightsBrightNow
    lightsBrightNow = int(15 * RPM/9000)
    if lightsBrightNow >= 15:
        lightsBrightNow = 14

    if lightsBrightNow != lightsBrightPrev:
        # Compute the number of packets that need to be sent.
        newLights = abs(lightsBrightNow - lightsBrightPrev)

        if lightsBrightNow < lightsBrightPrev:
            index = lightsBrightPrev-1
            while index >= lightsBrightNow:
                message = {"RPM" : "", "Index" : index, "Color" : 'gray'}
                messageList.append(message)
                index -= 1

        else:
            index = lightsBrightPrev
            while index < lightsBrightNow:
                if index < int(15/2):
                    color = 'green'
                elif index < int(15 * 3 / 4):
                    color = 'yellow'
                elif index < 15:
                    color = 'red'

                message = {"RPM" : "", "Index" : index, "Color" : color}
                messageList.append(message)
                index += 1

oilTemp = Bar("OILT", 40, 0, 20, 160)
oilPres = Bar("OILP", 40, 0, 0, 3.5)
waterTemp = Bar("WATERT", 40, 0, 20, 140)
battV = Bar("BATT", 40, 0, 10.5, 14.5)
packSOC = Bar("P_SOC", 40, 0, 0, 100)
packTemp = Bar("P_TEMP", 40, 0, 0, 55)


@asyncio.coroutine
def permanentServe(websocket, path):
    global lightsBrightNow
    global messageList
    global received
    global gearPrev
    global prev_V

    # Script will sit here and wait while the front end starts up and
    # kickstarts the control cycle.

    connectionName = yield from websocket.recv()
    print ("Connected to: " + connectionName)

    while True:

        # Connect to the hardware script via a UDP packet.
        hardware = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        hardware.settimeout(0.06)
        try:
            hardware.connect_ex(("localhost", 50000))

            # Send the hardware script an identification to query it for data.
            back_request = "Data Parser"
            hardware.send(back_request.encode('utf-8'))

            # Receive some data back.
            data = hardware.recv(1024).decode('utf-8')

            # Update each front-end object based on new data from the hardware.
            if data:
                parsed = json.loads(data)
                if "I_CURRENT" in parsed:
                    sendRPM(parsed["I_CURRENT"]*36)
                    for msg in messageList:
                        packet = json.dumps(msg)
                        yield from websocket.send(packet)
                    messageList = []
                if "RPM" in parsed:
                    sendRPM(parsed["RPM"])
                    for msg in messageList:
                        packet = json.dumps(msg)
                        yield from websocket.send(packet)
                    messageList = []
                if "OILT" in parsed:
                    if parsed["OILT"] != oilTemp.value:
                        oilTemp.updateBar(parsed["OILT"])
                        for msg in messageList:
                            packet = json.dumps(msg)
                            yield from websocket.send(packet)
                        messageList = []
                if "OILP" in parsed:
                    if parsed["OILP"] != oilPres.value:
                        oilPres.updateBar(parsed["OILP"])
                        for msg in messageList:
                            packet = json.dumps(msg)
                            yield from websocket.send(packet)
                        messageList = []
                if "WATERT" in parsed:
                    if parsed["WATERT"] != waterTemp.value:
                        waterTemp.updateBar(parsed["WATERT"])
                        for msg in messageList:
                            packet = json.dumps(msg)
                            yield from websocket.send(packet)
                        messageList = []
                if "BATT" in parsed:
                    if parsed["BATT"] != battV.value:
                        battV.updateBar(parsed["BATT"])
                        for msg in messageList:
                            packet = json.dumps(msg)
                            yield from websocket.send(packet)
                        messageList = []
                if "P_SOC" in parsed:
                    if parsed["P_SOC"] != packSOC.value:
                        packSOC.updateBar(parsed["P_SOC"])
                        for msg in messageList:
                            packet = json.dumps(msg)
                            yield from websocket.send(packet)
                        messageList = []
                if "P_TEMP" in parsed:
                    if parsed["P_TEMP"] != packTemp.value:
                        packTemp.updateBar(parsed["P_TEMP"])
                        for msg in messageList:
                            packet = json.dumps(msg)
                            yield from websocket.send(packet)
                        messageList = []
                if "ANG_V" in parsed:
                    if parsed["ANG_V"] != prev_V:
                        packet = json.dumps({"ANG_V" : parsed["ANG_V"]})
                        yield from websocket.send(packet)
                        prev_V = parsed["ANG_V"]
                        messageList = []
                if "GEAR" in parsed:
                    if parsed["GEAR"] != gearPrev:
                        # Just need to translate gear 0 to be Neutral.
                        if parsed["GEAR"] == 0:
                            packet = json.dumps({"GEAR" : 69}) # April Fools!
                            #packet = json.dumps({"GEAR" : 'N'})
                            yield from websocket.send(packet)
                        else:
                            packet = json.dumps({"GEAR" : parsed["GEAR"]})
                            yield from websocket.send(packet)
                        gearPrev = parsed["GEAR"]
                        messageList = []
                if "DPAD" in parsed:
                    # Make sure that if DPAD exists in the data packet, it actually
                    # has a new value to deal with.
                    if parsed["DPAD"] != "":
                        packet = json.dumps({"DPAD" : parsed["DPAD"]})
                        yield from websocket.send(packet)
                        messageList = []
            time.sleep(0.0001)
        except socket.timeout:
            print ("Socket timed out. Continuing to try again.")
            hardware.shutdown(socket.SHUT_RDWR)
            hardware.close()
        except socket.error:
            print ("Socket was abruptly closed. Continuing to try again.")
            #hardware.shutdown(socket.SHUT_RDWR)
            hardware.close()


# Create the websocket server routine.
start_server = websockets.serve(permanentServe, "localhost", 8765)

# Spin up the server routine.
asyncio.get_event_loop().run_until_complete(start_server)

# Catch if the server routine crashes and not exit the script violently.
asyncio.get_event_loop().run_forever()
