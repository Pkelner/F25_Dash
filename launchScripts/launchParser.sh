#!/bin/bash

echo "Launching the Parser"

cd /home/dash/F25_Dash/
python3 parser.py &
echo $! >> /tmp/parser.pid

echo "Parser launched."
