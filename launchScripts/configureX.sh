#!/bin/bash

DISPLAY=:0 unclutter -idle 0 &
echo $! >> /tmp/frontend.pid

DISPLAY=:0 xset s off
DISPLAY=:0 xset -dpms
DISPLAY=:0 xset s noblank
