#!/bin/bash
# This script will launch the frontend display.

echo "Launching the frontend"

xinit &
echo $! >> /tmp/frontend.pid

cd /home/dash/F25_Dash/nwjs/
DISPLAY=:0 ./dash_bin &
echo $! >> /tmp/frontend.pid

sleep 0.5

echo "GUI Launched"
